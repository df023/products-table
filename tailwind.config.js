module.exports = {
  theme: {
    colors: {
      gray: {
        100: "#FBFBFB",
        200: "#F8F9FA",
        300: "#EDEDED",
        400: "#D5DAE0",
        500: "#C6CBD4",
        600: "#5B5E77",
        700: "#3D374A",
        800: "#282136"
      },
      red: "#E55B5B",
      green: {
        light: "rgba(0, 161, 30, 0.07) !important",
        default: "#00A11E"
      },
      white: "#FFFFFF"
    },
    fontSize: {
      14: "1.4rem",
      32: "3.2rem"
    },
    spacing: {
      0: "0",
      8: "0.8rem",
      9: "0.9rem",
      10: "1rem",
      16: "1.6rem",
      20: "2rem",
      24: "2.4rem",
      30: "3rem",
      32: "3rem",
      60: "6rem",
      80: "8rem"
    },
    borderRadius: {
      none: "0",
      xs: "0.1rem",
      sm: "0.2rem",
      default: "0.4rem"
    },
    boxShadow: {
      default: "0px 2px 8px rgba(0, 0, 0, 0.16)"
    },
    minWidth: {
      full: "100%",
      94: "9.4rem",
      150: "15rem",
      250: "25rem",
      280: "28rem"
    },
    minHeight: {
      50: "5rem",
      80: "8rem",
      100: "10rem"
    },
    maxWidth: {
      1180: "118rem"
    }
  },
  variants: {},
  plugins: [
    function({ addUtilities, config, e }) {
      const betweenChildren = {};

      for (const [key, value] of Object.entries(config("theme.spacing"))) {
        betweenChildren[
          `.${e(`between-children-${key}`)} > *:not(:last-child)`
        ] = {
          marginRight: value
        };
        betweenChildren[
          `.${e(`v-between-children-${key}`)} > *:not(:last-child)`
        ] = {
          marginBottom: value
        };
      }

      addUtilities(betweenChildren);
    }
  ]
};
