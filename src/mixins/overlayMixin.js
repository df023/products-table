export default function overlayMixin(visibilityFieldName, refElName) {
  return {
    data() {
      return {
        [visibilityFieldName]: false
      };
    },
    watch: {
      [visibilityFieldName](visibility) {
        if (visibility) {
          document.addEventListener("click", this.globalCloseHandler);
        } else {
          document.removeEventListener("click", this.globalCloseHandler);
        }
      }
    },
    methods: {
      globalCloseHandler($e) {
        const refEl = this.$refs[refElName];

        for (const el of $e.path) {
          if (el === refEl) return;
        }

        this[visibilityFieldName] = false;
      }
    }
  };
}
