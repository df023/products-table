import { getProducts, deleteProducts } from "./request";

export default {
  getProducts,
  deleteProducts
};
