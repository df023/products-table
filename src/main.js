import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import SvgSprite from "vue-svg-sprite";

Vue.use(SvgSprite, {
  url: require("./assets/icons.svg")
});

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
