import { SHOW_ERROR_DURATION } from "@/utils/constants";

const state = () => ({
  list: []
});

let index = 0;

const mutations = {
  addError(state, error) {
    index += 1;
    const id = index;

    state.list.push({ id, ...error });
    setTimeout(() => {
      state.list = state.list.filter(error => error.id !== id);
    }, SHOW_ERROR_DURATION);
  }
};

export default {
  namespaced: true,
  state,
  mutations
};
