import { SORT_ORDER } from "@/utils/constants";

const state = () => ({
  productsList: [],
  currentPage: 1,
  perPage: 10,
  sortSettings: {
    field: "",
    order: SORT_ORDER.ASC
  }
});

const getters = {
  getProductsForPage({ perPage, currentPage, productsList }) {
    const offset = perPage * (currentPage - 1);
    return productsList.slice(offset, offset + perPage);
  },
  maxItems(state) {
    return state.productsList.length;
  },
  maxPage({ perPage }, { maxItems }) {
    return Math.ceil(maxItems / perPage);
  }
};

const mutations = {
  setProductsList(state, list) {
    state.productsList = list;
  },
  deleteProducts(state, productsToDelete) {
    state.productsList = state.productsList.filter(
      product => !productsToDelete.includes(product.id)
    );
  },
  setPage(state, pageNum) {
    state.currentPage = pageNum;
  },
  setPerPage(state, perPage) {
    state.perPage = perPage;
    state.currentPage = 1;
  },
  setSortSettings(state, payload) {
    state.sortSettings = { ...state.sortSettings, ...payload };

    const { field, order } = state.sortSettings;

    state.productsList.sort((p1, p2) => {
      const value1 = p1[field];
      const value2 = p2[field];

      if (value1 === value2) return 0;
      if (order == SORT_ORDER.ASC) return value1 > value2 ? 1 : -1;

      return value1 < value2 ? 1 : -1;
    });
  }
};

const actions = {
  async loadProducts({ commit }) {
    try {
      const products = await this.$api.getProducts();
      commit("setProductsList", products);
      commit("setSortSettings", { field: "product", order: SORT_ORDER.ASC });
    } catch (e) {
      commit(
        "errors/addError",
        {
          message: "Error loading products list"
        },
        { root: true }
      );
      return false;
    }

    return true;
  },
  async deleteProducts({ commit }, payload) {
    try {
      await this.$api.deleteProducts(payload);
      commit("deleteProducts", payload);
    } catch (e) {
      commit(
        "errors/addError",
        {
          message: "Error deleting products"
        },
        { root: true }
      );
      return false;
    }

    return true;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
