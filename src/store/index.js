import Vue from "vue";
import Vuex from "vuex";
import api from "@/api";
import products from "./products";
import errors from "./errors";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    products,
    errors
  }
});

store.$api = api;

export default store;
