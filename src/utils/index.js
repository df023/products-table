export function pluralize(n, variants) {
  let variant;
  if (n === 1) {
    variant = variants[0];
  } else {
    variant = variants[1];
  }

  return `${n} ${variant}`;
}
