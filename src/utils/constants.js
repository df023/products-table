// generic error message duration in ms
export const SHOW_ERROR_DURATION = 2000;

export const SORT_ORDER = {
  ASC: 0,
  DESC: 1
};
